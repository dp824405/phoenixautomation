package com.concepts;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JavaJDBCConnector {

	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
		Connection connection = DriverManager.getConnection("jdbc:mysql://139.59.91.96:3306/SR_DEV", "produser",
				"qweQWe123!");
		Statement statement = connection.createStatement();
		ResultSet rs = statement.executeQuery("select * from mst_oem");
		int total = 0;
		while (rs.next()) {
			total = total + 1;
			System.out.println(rs.getString("name") + "\t" + rs.getString("code"));
		}
		System.out.println("total number of records = " + total);
	}

}
