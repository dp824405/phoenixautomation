package com.ui.tests;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ui.pages.LoginPage;
import com.ui.pojo.DashboardTablePOJO;

public class VerifyDashBoardPageTable {
	private WebDriver wd;
	private LoginPage page;
	DashboardTablePOJO requestedData;

	@BeforeMethod(description = "Setup the Browser and load the page")
	public void setup() {
		wd = new ChromeDriver();
		page = new LoginPage(wd);

		requestedData = new DashboardTablePOJO("Apple", "JOB_28184", "3546301197010282", "IPhone", "Iphone 11",
				"In Warrenty", "Pending For Job Assignment");

		requestedData = new DashboardTablePOJO("JOB_28184");
	}

	@Test
	public void verifyDashBoardPageTableTest() {
		Assert.assertEquals(page.doLogin("iamfd", "password").verifyIfTableEntriesArePresent(requestedData), true);
	}
}
