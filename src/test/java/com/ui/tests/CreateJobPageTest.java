package com.ui.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ui.pages.LoginPage;
import com.ui.pojo.CreateJobPOJO;
import com.util.TestUtility;

public class CreateJobPageTest {
	private WebDriver wd;
	private LoginPage page;

	private CreateJobPOJO data;

	@BeforeMethod(description = "Setup the Browser and load the page", alwaysRun =  true)
	public void setup() {
		wd = new ChromeDriver();
		page = new LoginPage(wd);
		// data = new CreateJobPOJO("Google", "Nexus" , "Neeta" , "Ellur" ,
		// "Neeta@gmail.com");
		data = TestUtility.createFakeData();
	}

	@Test(groups = {"sanity"})
	public void testLoginForWebSite() {
		page.doLogin("iamfd", "password").goToCreateJobPage().createJob(data);

	}
}
