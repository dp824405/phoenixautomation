package com.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.ui.pojo.CreateJobPOJO;
import com.util.BrowserUtil;

public class CreateJobPage extends BrowserUtil {

	private static final By OEM_LOCATOR = By.xpath("//mat-select[@placeholder=\"Select OEM\"]/..");
	private static final By PRODUCT_NAME_DROPDOWN_LOCATOR = By
			.xpath("//span[contains(text(), \"Select Product name\")]/../../../..");

	private static final By MODEL_NAME_DROPDOWN_LOCATOR = By
			.xpath("//mat-select[contains(@placeholder,'Select Model name')]");
	private static final By IMEI_TEXT_BOX_LOCATOR = By.xpath("//input[contains(@data-placeholder,'0123456789')]");
	private static final By PURCHASE_DATE_BOX_LOCATOR = By.xpath("//input[@data-placeholder='dd/mm/yyyy']");
	private static final By WARRANTY_DROPDOWN_LOCATOR = By.xpath("//mat-select[@placeholder='Select Warranty Status']");
	private static final By PROBLEM_DROPDOWN_LOCATOR = By.xpath("//mat-select[@placeholder='Select Problem']");
	private static final By REMARK_TEXT_BOX_LOCATOR = By.xpath("//input[@placeholder='Remarks']");
	private static final By FIRST_NAME_TEXT_BOX_LOCATOR = By.xpath("//input[@data-placeholder='First Name']");
	private static final By LAST_NAME_TEXT_BOX_LOCATOR = By.xpath("//input[@data-placeholder='Last Name']");
	private static final By CONTACT_TEXT_BOX_LOCATOR = By.xpath("//input[@data-placeholder='Contact No.']");
	private static final By EMAIL_TEXT_BOX_LOCATOR = By.xpath("//input[@data-placeholder='Email Id.']");
	private static final By FLAT_NUMBER_TEXT_BOX_LOCATOR = By.xpath("//input[@data-placeholder='Flat/Society No.']");
	private static final By APARTMENT_NAME_TEXT_BOX_LOCATOR = By.xpath("//input[@data-placeholder='Apartment Name']");
	private static final By LANDMARK_TEXT_BOX_LOCATOR = By.xpath("//input[@data-placeholder='Choose a Landmark']");
	private static final By STREET_NAME_TEXT_BOX_LOCATOR = By.xpath("//input[@data-placeholder='Street Name.']");
	private static final By AREA_NAME_TEXT_BOX_LOCATOR = By.xpath("//input[@data-placeholder='Area']");
	private static final By STATE_NAME_TEXT_BOX_LOCATOR = By.xpath("//input[@data-placeholder='Select State']");
	private static final By PIN_CODE_TEXT_BOX_LOCATOR = By.xpath("//input[@data-placeholder='Pincode']");
	private static final By SUBMIT_BUTTON_LOCATOR = By.xpath("//span[contains(text(),'Submit')]/..");
	private static final By JOB_TOAST_LOCATOR = By.xpath("	//span[contains(text(),\"Job created successfully\")]");

	public CreateJobPage(WebDriver wd) {
		super(wd);
		// TODO Auto-generated constructor stub
	}

	public void createJob(String oemName, String prodName, String fName, String lName, String emailAddress) {
		System.out.println("Create Job Code....");
		selectFromDropDown(OEM_LOCATOR, oemName);
		selectFromDropDown(PRODUCT_NAME_DROPDOWN_LOCATOR, prodName);
		enterText(FIRST_NAME_TEXT_BOX_LOCATOR, fName);
		enterText(LAST_NAME_TEXT_BOX_LOCATOR, lName);
		enterText(EMAIL_TEXT_BOX_LOCATOR, emailAddress);

	}

	public void createJob(CreateJobPOJO data) {
		//
		System.out.println("Create Job Code....");
		selectFromDropDown(OEM_LOCATOR, data.getOemName());
		selectFromDropDown(PRODUCT_NAME_DROPDOWN_LOCATOR, data.getModelName());
		enterText(FIRST_NAME_TEXT_BOX_LOCATOR, data.getCustomerFirstName());
		enterText(LAST_NAME_TEXT_BOX_LOCATOR, data.getCustomerLastName());
		enterText(EMAIL_TEXT_BOX_LOCATOR, data.getCustomerEmailAddress());
		enterText(CONTACT_TEXT_BOX_LOCATOR, data.getCustomerContactNumber());

	}
}
