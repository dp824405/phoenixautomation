package com.ui.pages;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.ui.pojo.DashboardTablePOJO;
import com.util.BrowserUtil;
import com.util.TestUtility;

public final class DashboardPage extends BrowserUtil {

	private static final By CREATE_JOB_LINK_LOCATOR = By.xpath("//span[contains(text(),\"Create Job\")]/../../..");
	private static final By USERNAME_ICON_LOCATOR = By
			.xpath("//mat-icon[@data-mat-icon-name=\"user-circle\"]/../../..");
	private static final By USERNAME_TEXT_LOCATOR = By.xpath("//span[contains(text(),\"Signed in as\")]/../span[2]");

	private static final By CREATED_JOB_BUTTON_LOCATOR = By
			.xpath("//div[contains(text(),\"Created today\")]/../div/button");

	private static final By TABLE_LOCATOR = By.tagName("mat-table");
	private static final By TABLE_ROW_LOCATOR = By.xpath(".//mat-row");
	private static final By TABLE_CELL_LOCATOR = By.xpath(".//mat-cell");

	private WebDriver wd;

	public DashboardPage(WebDriver wd) {
		super(wd);
		this.wd = wd;

	}

	public String getUserName() {
		clickOn(USERNAME_ICON_LOCATOR);
		String userName = getVisibleText(USERNAME_TEXT_LOCATOR);
		return userName;
	}

	public CreateJobPage goToCreateJobPage() {
		clickOn(CREATE_JOB_LINK_LOCATOR);
		CreateJobPage createJobPage = new CreateJobPage(wd);
		return new CreateJobPage(wd);
	}

//	public List<DashboardTablePOJO> getDetailsFromTable() {
//		clickOn(CREATED_JOB_BUTTON_LOCATOR);
//		List<DashboardTablePOJO> dataList = getListOfElements(TABLE_LOCATOR, TABLE_ROW_LOCATOR, TABLE_CELL_LOCATOR);
//		Iterator<DashboardTablePOJO> dataIterator = dataList.iterator();
//		while (dataIterator.hasNext()) {
//			System.out.println(dataIterator.next());
//		}
//		return dataList;
//	}

	public boolean verifyIfTableEntriesArePresent(DashboardTablePOJO data) {
		clickOn(CREATED_JOB_BUTTON_LOCATOR);
		List<DashboardTablePOJO> dataList = getListOfElements(TABLE_LOCATOR, TABLE_ROW_LOCATOR, TABLE_CELL_LOCATOR);
		return TestUtility.searchEntryInList(dataList, data);
	}
}