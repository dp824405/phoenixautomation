package com.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.util.BrowserUtil;

public final class LoginPage extends BrowserUtil {

	private static final By USER_NAME_TEXTBOX_LOCATOR = By.id("username");
	private static final By PASSWORD_TEXTBOX_LOCATOR = By.id("password");
	private static final By SIGN_BUTTON_LOCATOR = By.xpath("//span[contains(text(),\"Sign in\")]/../..");

	private WebDriver wd;

	public LoginPage(WebDriver wd) {
		super(wd);
		// TODO Auto-generated constructor stub
		this.wd = wd;
		goToWebSite("http://phoenix.testautomationacademy.in/sign-in");
		viewInFullScreen();

	}

	public DashboardPage doLogin(String userName, String password) {
		
		enterText(USER_NAME_TEXTBOX_LOCATOR, userName);
		enterText(PASSWORD_TEXTBOX_LOCATOR, password);
		clickOn(SIGN_BUTTON_LOCATOR);
		DashboardPage dashboardPage = new DashboardPage(wd);
		return dashboardPage;
	}
	
}
