package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnector {

	public static DBConnector d1;
	public static  Connection connection;
	private static final String DB_URL = "jdbc:mysql://139.59.91.96:3306/SR_DEV";
	private static final String USERNAME = "produser";
	private static final String PASSWORD = "qweQWe123!";

	private DBConnector() {
		try {
			connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static Connection getConnection() {

		if (d1 == null) {

			d1 = new DBConnector();
		}
		return connection;

	}
}
