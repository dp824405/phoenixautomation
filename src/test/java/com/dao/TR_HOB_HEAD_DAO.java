package com.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.db.pojo.HOB_HEAD_POJO;
import com.db.pojo.MST_OEM_POJO;

public class TR_HOB_HEAD_DAO {
	private Connection connection;
	private Statement statement;
	private HOB_HEAD_POJO data;

	public TR_HOB_HEAD_DAO() {
		// TODO Auto-generated constructor stub

		connection = DBConnector.getConnection();
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public HOB_HEAD_POJO getJobDetails(int id) throws SQLException {
		ResultSet rs = statement.executeQuery(
				"SELECT id, job_number, tr_customer_id, tr_customer_product_id, mst_service_location_id, mst_platform_id, "
				+ "mst_warrenty_status_id, mst_oem_id, repair_start_date, repair_end_date, created_at, modified_at, tr_job_detail_id\r\n"
						+ "FROM SR_DEV.tr_job_head\r\n" + "WHERE id=" + id + ";\r\n" + "");

		int total = 0;
		while (rs.next()) {
			total = total + 1;
			data = new HOB_HEAD_POJO(id, rs.getString("job_number"), rs.getInt("tr_customer_id"),
					rs.getInt("tr_customer_product_id"), rs.getInt("mst_service_location_id"),
					rs.getInt("mst_platform_id"), rs.getInt("mst_warrenty_status_id"),

					rs.getInt("mst_oem_id"),

					rs.getString("repair_start_date"), rs.getString("repair_end_date"), rs.getString("created_at"),
					rs.getString("modified_at"), rs.getInt("tr_job_detail_id"));

		}

		return data;
	}
}
