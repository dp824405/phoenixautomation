package com.api.tests;

import static com.util.TestUtility.convertToJson;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.lessThan;

import java.io.File;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.api.pojo.LoginRequestPOJO;

import io.restassured.http.Header;
import io.restassured.module.jsv.JsonSchemaValidator;
@Listeners(com.listeners.APIListeners.class)
public class LoginResponseSchema {
	static {
		baseURI = "http://139.59.91.96:9000/v1";

	}

	File jsonSchemaFile = new File(
			System.getProperty("user.dir") + "//src//test//resources//responseSchemas//loginResponseSchema.json");

	@Test(description = "test login api request", groups = {
			"sanity" }, dataProviderClass = com.dataProviders.LoginTestData.class, dataProvider = "LoginCredcsv"

	)

	public void loginAPITest(String username, String password) {

		Header myHeader = new Header("Content-Type", "application/json");
		LoginRequestPOJO loginPOJO = new LoginRequestPOJO(username, password);

		String token = given().when().header(myHeader).and().body(convertToJson(loginPOJO)).and().log().all()
				.post("/login").then().log().all().assertThat().statusCode(200).and().time(lessThan(1500L)).and()
				.body(JsonSchemaValidator.matchesJsonSchema(jsonSchemaFile)).and().body("message", equalTo("Success"))
				.and().extract().jsonPath().getString("data.token");
		System.out.println("-----------------------------------------");
		System.out.println(token);
	}
}
