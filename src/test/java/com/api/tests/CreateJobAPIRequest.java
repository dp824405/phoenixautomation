package com.api.tests;

import static com.util.TestUtility.convertToJson;
import static com.util.TestUtility.createJobRequestPOJO;
import static com.util.TestUtility.jobId;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.util.TestUtility;

import io.restassured.http.Header;
@Listeners(com.listeners.APIListeners.class)
public final class CreateJobAPIRequest {
	private Header h1;
	private Header h2;
	private int jobNumber;

	static {
		baseURI = "http://139.59.91.96:9000/v1";
	}

	@BeforeMethod(description = "Intializing the headers", alwaysRun = true)
	public void setup() {
		h1 = new Header("Content-type", "application/json");
		h2 = new Header("Authorization", TestUtility.generateTokenFor("fd"));

	}

	@Test(description = "test create job api requests generates job number", groups = { "sanity", "smoke", })

	public void createjobTest() {
		// TODO Auto-generated method stub

		jobId = given()
					.header(h1).and().header(h2)
					.body(convertToJson(createJobRequestPOJO())).log().all()
					.when().post("/job/create")
					.then().log().all()
					.assertThat().statusCode(200).and().body("message", equalTo("Job created successfully. "))
					.and().extract().jsonPath().getInt("data.id");
		System.out.println(jobNumber);

	}

	@Test(description = "Verify the Details from the d", groups = { "sanity", "smoke" }, dependsOnMethods = {
			"createjobTest" })

	public void validateEntriesInDB() {

	}

}
