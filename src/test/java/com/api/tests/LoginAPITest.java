package com.api.tests;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.lessThan;

import java.io.IOException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.api.pojo.LoginRequestPOJO;
import com.opencsv.exceptions.CsvException;
import com.util.TestUtility;

import io.restassured.http.Header;
@Listeners(com.listeners.APIListeners.class)
public class LoginAPITest {
	LoginRequestPOJO loginRequestPOJO;
	String jsonData;
	Header myHeader;
	Header myHeader2;

	@BeforeMethod(description = "Setting up Base URI, Header")

	public void setup() {
		baseURI = "http://139.59.91.96:9000";
		myHeader = new Header("Content-Type", "application/json");
	}

	@Test(description = "Verify if the User is able to Login into the Application via api ", groups = { "api", "sanity",
			"smoke", "e2e" }, dataProviderClass = com.dataProviders.LoginTestData.class, dataProvider = "LoginCredexcel",
			retryAnalyzer = com.listeners.RetryAnalyzer.class)
	public void loginAPITest(String username, String password) {
		loginRequestPOJO = new LoginRequestPOJO(username, password);
		jsonData = TestUtility.convertToJson(loginRequestPOJO);

		given().header(myHeader).and().body(jsonData).log().all().when().post("/v1/login").then().log().all()
				.assertThat().statusCode(200).and().body("message", equalTo("Success")).and().time(lessThan(2000L));
	}

}
