package com.listeners;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.util.BrowserUtil;
import com.util.TestUtility;

public class MyUITestListener implements ITestListener {

	private ExtentReports extentReports;
	private ExtentTest extentTest;
	private ExtentSparkReporter extentSparkReporter;

	@Override
	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub
		System.out.println(
				"******************* " + result.getMethod().getMethodName() + " Test Started ******************* ");
		System.out.println("******************* " + result.getMethod().getDescription() + " ******************* ");
		System.out.println(
				"******************* " + Arrays.toString(result.getMethod().getGroups()) + " ******************* ");
		extentTest = extentReports.createTest(result.getMethod().getMethodName());
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub
		System.out.println("******************* Test Success ******************* ");
		extentTest.pass("Test passed");
//		addDevLogs();
	}

//	private void addDevLogs() {
//		// TODO Auto-generated method stub
//		BrowserUtil.dev.addListener(Network.requestWillBeSent(), requestConsumer -> {
//			Request request = requestConsumer.getRequest();
//			System.out.println(request.getUrl());
//			System.out.println(request.getMethod());
//			System.out.println(request.getHeaders());
//			System.out.println(request.getPostData());
//			extentTest.log(Status.INFO, request.getUrl());
//			extentTest.log(Status.INFO, request.getMethod());
//			extentTest.log(Status.INFO, request.getHeaders().toString());
//			extentTest.log(Status.INFO, request.getPostData().toString());
//
//		});
//
//		BrowserUtil.dev.addListener(Network.responseReceived(), responseConsumer -> {
//			Response response = responseConsumer.getResponse();
//			System.out.println("-----------" + response.getUrl());
//			System.out.println("-----------" + response.getStatus());
//			extentTest.log(Status.INFO, response.getUrl());
//			extentTest.log(Status.INFO, response.getStatusText());
//			extentTest.log(Status.INFO, response.getHeaders().toString());
//
//		});
//	}

	@Override
	public void onTestFailure(ITestResult result) {
		// TODO Auto-generated method stub
		System.out.println("******************* Test Failed ******************* ");
		extentTest.fail("Test failed");
		// CODE??
		extentTest.addScreenCaptureFromPath(BrowserUtil.takeScreenShot(result.getMethod().getMethodName()));
//		addDevLogs();
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub
		System.out.println("******************* Test Skipped ******************* ");
		extentTest.skip("Test Skipped");
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStart(ITestContext context) {

		// TODO Auto-generated method stub
		System.out.println("******************* TestSuite Started ******************* ");
		File reportDirectory = new File(System.getProperty("user.dir") + "/report");
		try {
			FileUtils.forceMkdir(reportDirectory);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		extentSparkReporter = new ExtentSparkReporter(
				System.getProperty("user.dir") + "/report/report_" + TestUtility.getTime() + ".html");
		extentReports = new ExtentReports();
		extentReports.attachReporter(extentSparkReporter);

	}

	@Override
	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
		System.out.println("TestSuite Finished");
		extentReports.flush();
	}

}
