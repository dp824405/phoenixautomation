package com.dataProviders;

import java.io.IOException;
import java.util.Iterator;

import org.testng.annotations.DataProvider;

import com.util.TestUtility;

public class LoginTestData {

	@DataProvider(name = "LoginCredcsv")
	public Iterator<String[]> loginDPcsv() {
		return TestUtility.readcsv("loginCredentials.csv");
	}
	
	@DataProvider(name = "LoginCredexcel")
	public String[][] loginDPexcel() throws IOException {
		return TestUtility.readexcel("loginCredentials.xlsx");
	}

}
