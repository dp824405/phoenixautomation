package com.util;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.api.pojo.CreateJOBResponse;
import com.api.pojo.CreateJobRequestPOJO;
import com.api.pojo.Customer;
import com.api.pojo.Customer_Address;
import com.api.pojo.Customer_Product;
import com.api.pojo.LoginRequestPOJO;
import com.api.pojo.Problem;
import com.github.javafaker.Faker;
import com.google.gson.Gson;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import com.ui.pojo.DashboardTablePOJO;
import com.util.IOem;

import io.restassured.http.Header;

public class TestUtility {
	public static int jobId;

	public static String convertToJson(Object refVariable) {
		Gson gson = new Gson();
		String data = gson.toJson(refVariable);
		return data;
	}

	public static CreateJOBResponse convertJSONtoCreateJOBResponsePOJO(String jsonData) {
		Gson gson = new Gson();
		CreateJOBResponse pojo = gson.fromJson(jsonData, CreateJOBResponse.class);
		return pojo;
	}

	public static CreateJobRequestPOJO createJobRequestPOJO() {
		Faker faker = new Faker();
		String fName = faker.name().firstName();
		String lName = faker.name().lastName();
		String phoneNumber = faker.phoneNumber().cellPhone();
		String emailAddress = faker.internet().emailAddress();

		String aptNumber = faker.address().buildingNumber();
		String aptName = faker.address().streetName();
		String streetName = faker.address().streetName();

		String IMEINumber = faker.numerify("8##############"); // 823435434234535
		Customer customer = new Customer(fName, lName, phoneNumber, null, emailAddress, null);
		Customer_Address address = new Customer_Address(aptNumber, aptName, streetName, "in orbit mall", "Link Road",
				"411045", "India", "Maharashtra");
		Customer_Product product = new Customer_Product("2023-06-10T18:30:00.000Z", IMEINumber, IMEINumber, IMEINumber,
				"2023-06-10T18:30:00.000Z", 1, 1);
		Problem[] deviceProblem = new Problem[1];
		deviceProblem[0] = new Problem(1, "Phone not working");

		CreateJobRequestPOJO createJobRequestPOJO = new CreateJobRequestPOJO(0, 2, 1, 1, customer, address, product,
				deviceProblem);

		return createJobRequestPOJO;
	}

	public static String generateTokenFor(String role) {

		LoginRequestPOJO loginRequestPOJO = null;
		if (role.equalsIgnoreCase("fd")) {
			loginRequestPOJO = new LoginRequestPOJO("iamfd", "password");
		} else if (role.equalsIgnoreCase("sup ")) {
			loginRequestPOJO = new LoginRequestPOJO("iamsup", "password");
		}

		else if (role.equalsIgnoreCase("eng")) {
			loginRequestPOJO = new LoginRequestPOJO("iameng", "password");
		}

		else if (role.equalsIgnoreCase("qc")) {
			loginRequestPOJO = new LoginRequestPOJO("iamqc", "password");
		}

		else if (role.equalsIgnoreCase("fst")) {
			loginRequestPOJO = new LoginRequestPOJO("iamfst3", "password");
		}

		else if (role.equalsIgnoreCase("cc")) {
			loginRequestPOJO = new LoginRequestPOJO("iamcc", "password");
		} else {
			System.err.print("Invalid role...Please enter role as fd,sup,qc,cc,fst,eng");
		}

		String jsonData = TestUtility.convertToJson(loginRequestPOJO);
		String token = given().when().header(new Header("Content-type", "application/json")).and().body(jsonData).and()
				.log().all().post("/login").then().extract().jsonPath().getString("data.token");

		return token;
	}

	public static String generateTokenFor(Role role) {

		LoginRequestPOJO loginRequestPOJO = null;
		if (role == Role.FD) {
			loginRequestPOJO = new LoginRequestPOJO("iamfd", "password");
		} else if (role == Role.SUP) {
			loginRequestPOJO = new LoginRequestPOJO("iamsup", "password");
		}

		else if (role == Role.ENG) {
			loginRequestPOJO = new LoginRequestPOJO("iameng", "password");
		}

		else if (role == Role.QC) {
			loginRequestPOJO = new LoginRequestPOJO("iamqc", "password");
		}

		else if (role == Role.FST) {
			loginRequestPOJO = new LoginRequestPOJO("iamfst3", "password");
		}

		else if (role == Role.CC) {
			loginRequestPOJO = new LoginRequestPOJO("iamcc", "password");
		}
		Header myHeader = new Header("Content-Type", "application/json");
		String jsonData = TestUtility.convertToJson(loginRequestPOJO);
		String data = given().header(myHeader).and().body(jsonData).log().all().when().post("/v1/login").then().log()
				.all().extract().path("data.token");

		return data;
	}

	public static Iterator<String[]> readcsv(String filename) {
		File csvfile = new File(System.getProperty("user.dir") + "//src//test//resources//testData//" + filename);
		FileReader fileReader;
		CSVReader csvReader;
		List<String[]> dataList = null;
		try {
			fileReader = new FileReader(csvfile);
			csvReader = new CSVReader(fileReader);
			dataList = csvReader.readAll();
//			for (String[] myData : dataList) {
//				for (String data : myData) {
//					System.out.print(data + " ");
//				}
//				System.out.println();
//			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException | CsvException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Iterator<String[]> dataIterator = dataList.iterator();
		dataIterator.next();
		return dataIterator;
	}

	public static String[][] readexcel(String filename) throws IOException {
		XSSFWorkbook myWorkbook = new XSSFWorkbook(
				System.getProperty("user.dir") + "//src//test//resources//testData//" + filename);
		XSSFSheet mySheet = myWorkbook.getSheetAt(0);
		int lastRowIndex = mySheet.getLastRowNum();
		XSSFRow myHeader = mySheet.getRow(0);
		int numberOfColumn = myHeader.getLastCellNum();

		String myData[][] = new String[lastRowIndex][numberOfColumn];
		for (int row = 1; row <= lastRowIndex; row++) {
			for (int col = 0; col < numberOfColumn; col++) {
				XSSFRow myRow = mySheet.getRow(row);
				XSSFCell myCell = myRow.getCell(col);
				myData[row - 1][col] = myCell.getStringCellValue();
			}
		}
		return myData;
	}

	public static boolean searchEntryInList(List<DashboardTablePOJO> dataList, DashboardTablePOJO data) {
		Iterator<DashboardTablePOJO> dataIterator = dataList.iterator();
		boolean status = false;
		while (dataIterator.hasNext()) {
			DashboardTablePOJO dataFromTable = dataIterator.next();
			System.out.println(dataFromTable);
			System.out.println(data);
			System.out.println("---------------------------------");
			if (dataFromTable.equals(data)) {
				status = true;
				break;
			} else {
				status = false;
			}
		}
		return status;
	}

	public static String getTime() {
		Date date = new Date();
		System.out.println(date.toString());

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY HH-mm");
		String formatedDate = sdf.format(date);

		return formatedDate;
	}

	public static com.ui.pojo.CreateJobPOJO createFakeData() {

		Faker faker = new Faker();
		com.ui.pojo.CreateJobPOJO data = new com.ui.pojo.CreateJobPOJO(IOem.GOOGLE, "Nexus", faker.name().firstName(),
				faker.name().lastName(), faker.internet().emailAddress(), faker.numerify("98########"));

		return data;
	}

}
